class Outcome
  attr_reader :result, :errors

  def initialize(result, errors = [])
    @result = errors.present? ? nil : result
    @errors = errors
    @success = success?
  end

  def success?
    @errors.blank?
  end

  def failure?
    !success?
  end
end
