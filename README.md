##  [Demo](https://xing-test.herokuapp.com/).
** **
> Demo available [here](https://xing-test.herokuapp.com/)

> *Might take a **couple more seconds** to load the first time, because its an hobby server on heroku and these servers are put to sleep after a while with no activity*

## The challenge

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.
A rover’s position and location is represented by a combination of x and y co- ordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.
In order to control a rover, NASA sends a simple string of letters. The possible letters are ‘L’, ‘R’ and ‘M’. ‘L’ and ‘R’ makes the rover spin 90 degrees left or right respectively, without moving from its current spot. ‘M’ means move forward one grid point, and maintain the same Heading.

Assume that the square directly North from (x, y) is (x, y+1).

##INPUT:
The first line of input is the upper-right coordinates of the plateau, the lower- left coordinates are assumed to be 0,0. The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input.The first line gives the rover’s position, and the second line is a series of instructions telling the rover how to explore the plateau. The position is made up of two integers and a letter separated by spaces, corresponding to the x and y co-ordinates and the rover’s orientation. Each rover will be finished sequentially, which means that the second rover won’t start to move until the first one has finished moving.

##OUTPUT
The output for each rover should be its final co-ordinates and heading.

The output for each rover should be its final co-ordinates and heading.
# Test Input:

5 5

1 2 N

LMLMLMLMM

3 3 E

MMRMMRMRRM

# Expected Output:

1 3 N

5 1 E

## Summary

Below are things I would like to improve upon:

- Handling of errors.
- Perhaps Sinatra would have been a better framework for this in terms of its being light weight
- Overall architecture could improve.

Listed below are things I enjoyed, it was an exciting experience.

- Determining how to make the turns.
- Working out the flow of the app.
- TDD

##Considerations

- Each rover could have logs for each movement wether successful or not.
- Change rover instructions and execute after change.
- List of all missions


