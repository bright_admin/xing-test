class CreatePlateaus < ActiveRecord::Migration[5.2]
  def change
    create_table :plateaus do |t|
      t.integer :max_x
      t.integer :max_y
      t.integer :min_y, default: 0
      t.integer :min_x, default: 0
      t.timestamps
    end
  end
end
