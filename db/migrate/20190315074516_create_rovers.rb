class CreateRovers < ActiveRecord::Migration[5.2]
  def change
    create_table :rovers do |t|
      t.integer :position_x
      t.integer :position_y
      t.string  :orientation
      t.text    :instructions
      t.references :mission
      t.timestamps
    end
  end
end
