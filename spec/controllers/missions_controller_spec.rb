require 'rails_helper'

RSpec.describe MissionsController, type: :controller do
  describe 'new' do
    context 'rendering' do
      before do
        get :new
      end

      it 'renders new template' do
        expect(response).to render_template(:new)
      end

      it 'has a status 200' do
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'show' do
    let(:mission) { FactoryBot.create(:mission) }

    context 'rendering' do
      before do
        get :show, params: { id: mission.id }
      end

      it 'renders show' do
        expect(response).to render_template(:show)
      end
    end

    context 'mission found' do
      before do
        get :show, params: { id: mission.id }
      end

      it 'assigns mission to @mission' do
        expect(assigns(:mission)).to eq(mission)
      end

      it 'renders show' do
        expect(response).to render_template(:show)
      end
    end

    context 'mission not found' do
      before do
        get :show, params: { id: 0 }
      end

      it 'redirects back' do
        expect(response).to redirect_to(root_path)
      end

      it 'has flash error' do
        expect(flash[:error]).to be_present
      end
    end
  end

  describe 'create' do
    context 'success' do
      let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }

      subject(:post_to_create) do
        post :create, params: { mission_info: info }
      end

      it 'redirects to mission show page' do
        post_to_create
        expect(response.status).to redirect_to(mission_path(Mission.last))
      end

      it 'creates plateau' do
        expect do
          post_to_create
        end.to change(Plateau, :count).by(1)
      end

      it 'creates a mission' do
        expect do
          post_to_create
        end.to change(Mission, :count).by(1)
      end

      it 'creates a rovers' do
        expect do
          post_to_create
        end.to change(Rover, :count).by(2)
      end
    end

    context 'failure' do
      let(:info) { 'dsadadsaddsa' }

      before do
        post :create, params: { mission_info: info }
      end

      it 'redirects to back' do
        expect(response).to redirect_to(root_path)
      end

      it 'has flash error' do
        expect(flash[:error]).to be_present
      end
    end
  end
end
