
FactoryBot.define do
  factory :rover do
    position_x { 1 }
    position_y { 2 }
    orientation { 'N' }
    association :mission, factory: :mission
  end
end
