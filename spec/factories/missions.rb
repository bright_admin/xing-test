
FactoryBot.define do
  factory :mission do
    name { 'Mission random' }
    association :plateau, factory: :plateau
  end
end
