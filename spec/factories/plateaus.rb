
FactoryBot.define do
  factory :plateau do
    max_x { 5 }
    max_y { 5 }
  end
end
