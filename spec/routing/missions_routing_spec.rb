# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MissionsController, type: :routing do
  describe 'GET #new' do
    it 'routes correctly to new' do
      expect(get: '/missions/new').to route_to('missions#new')
    end
  end

  describe 'POST #create' do
    it 'routes correctly to create' do
      expect(post: '/missions').to route_to('missions#create')
    end
  end

  describe 'GET #index' do
    it 'routes correctly to index' do
      expect(get: '/missions').to route_to('missions#index')
    end
  end

  describe 'GET #show' do
    it 'routes correctly to show' do
      expect(get: '/missions/1').to route_to('missions#show', id: '1')
    end
  end
end
