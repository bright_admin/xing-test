require 'rails_helper'
require 'outcome'

RSpec.describe Outcome, type: :services do
  describe 'initialize' do
    context 'errors' do
      subject(:outcome) { described_class.new('result', ['error']) }

      it 'result is nil' do
        expect(outcome.result).to be_nil
      end
    end

    context 'no errors' do
      subject(:outcome) { described_class.new('good', []) }

      it 'result to be present' do
        expect(outcome.result).to eq('good')
      end
    end
  end

  describe 'success?' do
    context 'no errors' do
      subject(:outcome) { described_class.new('good', []) }

      it 'returns true' do
        expect(outcome.success?).to be_truthy
      end
    end

    context 'errors' do
      subject(:outcome) { described_class.new('result', ['error']) }

      it 'returns false' do
        expect(outcome.success?).to be_falsey
      end
    end
  end

  describe 'failure?' do
    context 'no errors' do
      subject(:outcome) { described_class.new('good', []) }

      it 'returns false' do
        expect(outcome.failure?).to be_falsey
      end
    end

    context 'errors' do
      subject(:outcome) { described_class.new('result', ['error']) }

      it 'returns true' do
        expect(outcome.failure?).to be_truthy
      end
    end
  end
end
