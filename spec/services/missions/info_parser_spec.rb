# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Missions::InfoParser, type: :services do
  let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }

  describe 'initialize' do
    let(:info_parser) { described_class.new(info) }

    context 'info' do
      it 'returns info' do
        expect(info_parser.info).to eq(info)
      end
    end
  end

  describe 'rovers' do
    let(:info_parser) { described_class.new(info) }
    let(:parsed_rovers) do
      [
        { position_x: 1, position_y: 2, orientation: 'N', instructions: 'LMLMLMLMM' },
        { position_x: 3, position_y: 3, orientation: 'E', instructions: 'MMRMMRMRRM' }
      ]
    end

    context 'rovers info parse ok' do
      subject(:rovers) { info_parser.rovers }

      it 'returns array rover info(hash)' do
        expect(rovers).to eq(parsed_rovers)
      end
    end
  end

  describe 'plateau_bounds' do
    let(:info_parser) { described_class.new(info) }

    context 'assign plateau bounds' do
      subject(:plateau_bounds) { info_parser.plateau_bounds }

      it 'returns object of max_x and max_y' do
        expect(plateau_bounds).to eq(max_x: 5, max_y: 5)
      end
    end
  end
end
