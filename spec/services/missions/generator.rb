# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Missions::Generator, type: :services do
  describe 'initialize' do
    let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }

    context 'parses info' do
      let(:mission_generator) { described_class.new(info) }

      it 'returns has passed info' do
        expect(mission_generator.info).to be_present
      end
    end
  end

  describe 'generate_mission' do
    context 'correct data' do
      subject(:generate_mission) { mission_generator.generate_mission }

      let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }
      let(:mission_generator) { described_class.new(info) }

      it 'creates plateau' do
        expect do
          generate_mission
        end.to change(Plateau, :count).by(1)
      end

      it 'creates a mission' do
        expect do
          generate_mission
        end.to change(Mission, :count).by(1)
      end

      it 'creates a rovers' do
        expect do
          generate_mission
        end.to change(Rover, :count).by(2)
      end
    end

    context 'invalid data' do
      subject(:generate_mission) { mission_generator.generate_mission }

      let(:mission_generator) { described_class.new('afafsadafsasad') }

      it 'does not creates plateau' do
        expect do
          generate_mission
        end.to change(Plateau, :count).by(0)
      end

      it 'does not creates mission' do
        expect do
          generate_mission
        end.to change(Mission, :count).by(0)
      end

      it 'does not creates rovers' do
        expect do
          generate_mission
        end.to change(Rover, :count).by(0)
      end

      it 'has errors' do
        generate_mission
        expect(mission_generator.errors.any?).to be_truthy
      end

      it 'does not change prepared_to_deploy' do
      end
    end
  end

  describe 'create_plateau' do
    context 'correct data' do
      subject(:create_plateau) { mission_generator.create_plateau }

      let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }
      let(:mission_generator) { described_class.new(info) }

      it 'creates plateau' do
        expect do
          create_plateau
        end.to change(Plateau, :count).by(1)
      end
    end

    context 'invalid data' do
      subject(:create_plateau) { mission_generator.create_plateau }

      let(:mission_generator) { described_class.new('') }

      it 'does not creates plateau' do
        expect do
          create_plateau
        end.to change(Plateau, :count).by(0)
      end

      it 'has errors' do
        create_plateau
        expect(mission_generator.errors).to include('Max x is not a number')
      end
    end
  end

  describe 'create_mission' do
    context 'with valid data' do
      subject(:create_mission) { mission_generator.create_mission }

      let(:info) { "5 5\n1 2 N\nL M L M L M L M M\n3 3 E\nM M R M M R M R R M" }
      let(:mission_generator) { described_class.new(info) }

      before { mission_generator.create_plateau }

      it 'creates mission' do
        expect do
          create_mission
        end.to change(Mission, :count).by(1)
      end
    end

    context 'invalid data' do
      subject(:create_mission) { mission_generator.create_mission }

      let(:mission_generator) { described_class.new('') }

      it 'does not creates mission' do
        expect do
          create_mission
        end.to change(Mission, :count).by(0)
      end

      it 'has errors' do
        create_mission
        expect(mission_generator.errors).to include('Plateau must exist')
      end
    end
  end
end
