require 'rails_helper'

RSpec.describe Mission, type: :model do
  context 'factory' do
    let(:mission) { FactoryBot.build(:mission) }

    it 'has valid factory' do
      expect(mission).to be_valid
    end
  end

  context 'has_many rovers' do
    subject(:mission_rovers) { mission.rovers }

    let(:mission) { FactoryBot.create(:mission) }
    let!(:rovers) { FactoryBot.create_list(:rover, 4, mission: mission) }

    it 'returns rovers' do
      expect(mission_rovers).to eq(rovers)
    end
  end

  context 'after_create plateau' do
    subject(:mission_plateu) { mission.plateau }

    let(:mission) { FactoryBot.create(:mission, plateau: plateau) }
    let!(:plateau) { FactoryBot.create(:plateau) }

    it 'returns rovers' do
      expect(mission_plateu).to eq(plateau)
    end
  end

  context 'before_create set mission name' do
    subject(:create_mission) { FactoryBot.create(:mission, name: nil) }

    it 'sets name' do
      expect(create_mission.name).to eq("Mission 1")
    end
  end
end
