require 'rails_helper'

RSpec.describe Plateau, type: :model do
  context 'factory bot' do
    let(:plateau) { FactoryBot.create(:plateau) }

    it 'is valid' do
      expect(plateau).to be_valid
    end
  end

  %w[max_y max_x].each do |attr|
    context "#{attr} is less than 0" do
      let(:plateau) do
        FactoryBot.build(:plateau).tap{ |plateau| plateau[attr.to_sym] = -1 }
      end

      it 'is invalid' do
        expect(plateau).to be_invalid
      end
    end
  end
end
