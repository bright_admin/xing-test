require 'rails_helper'

RSpec.describe Rover, type: :model do
  context 'factory' do
    let(:rover) { FactoryBot.build(:rover) }

    it 'is valid' do
      expect(rover).to be_valid
    end
  end

  context 'belongs_to mission' do
    subject(:association) { rover.mission }

    let(:mission) { FactoryBot.create(:mission) }
    let(:rover) { FactoryBot.create(:rover, mission: mission) }

    it 'returns a mission' do
      expect(association).to eq(mission)
    end
  end

  describe 'validations' do
    %w[position_x position_y orientation].each do |attribute|
      context "#{attribute} is not present" do
        let(:rover) do
          FactoryBot.build(:rover).tap { |rover| rover[attribute.to_sym] = nil }
        end

        it 'is invalid' do
          expect(rover).to be_invalid
        end
      end
    end
  end

  describe 'move_forward' do
    subject(:move_forward) { rover.move_forward }

    before { move_forward }

    context 'out of plateau bounds' do
      let(:plateau) { FactoryBot.create(:plateau, max_x: 4, max_y: 3) }
      let(:mission) { FactoryBot.create(:mission, plateau: plateau) }
      let(:rover) { FactoryBot.create(:rover, mission: mission, position_x: 4, position_y: 3, orientation: 'E') }

      it 'rolls back' do
        expect(rover.errors.full_messages.first).to eq('Rover out of bounds')
      end
    end

    { W: -1, E: 1 }.each do |key, value|
      context "moved x#{value}" do
        let(:rover) { FactoryBot.create(:rover, position_x: 2, position_y: 3, orientation: key) }
        let(:position_x) { 2 }

        it 'returns position_x as 2' do
          expect(rover.position_x).to eq(position_x + value)
        end
      end
    end

    { S: -1, N: 1 }.each do |key, value|
      context "moved y#{value}" do
        let(:rover) { FactoryBot.create(:rover, position_x: 2, position_y: 3, orientation: key) }
        let(:position_y) { 3 }
        let(:orientation) { key }

        it 'returns position_x as 2' do
          expect(rover.position_y).to eq(position_y + value)
        end
      end
    end
  end

  describe 'turn_left' do
    subject(:turn_left) { rover.turn_left }

    let(:rover) { FactoryBot.create(:rover, position_x: 1, position_y: 2, orientation: current_orientation) }

    before { turn_left }

    { N: 'W', W: 'S', S: 'E', E: 'N' }.each do |orientation, new_orientation|
      context "orientation is #{orientation}" do
        let(:current_orientation) { orientation }
        it "changes to #{new_orientation}" do
          expect(rover.orientation).to eq(new_orientation)
        end
      end
    end
  end

  describe 'turn_right' do
    subject(:turn_right) { rover.turn_right }

    let(:rover) { FactoryBot.create(:rover, position_x: 1, position_y: 2, orientation: current_orientation) }

    before { turn_right }

    { N: 'E', E: 'S', S: 'W', W: 'N' }.each do |orientation, new_orientation|
      context "orientation is #{orientation}" do
        let(:current_orientation) { orientation }
        it "changes to #{new_orientation}" do
          expect(rover.orientation).to eq(new_orientation)
        end
      end
    end
  end

  describe 'valid_instruction?' do
    let(:rover) { FactoryBot.create(:rover) }

    context 'not valid' do
      subject(:valid_instruction) { rover.valid_instruction?('A') }

      it 'returns false' do
        expect(valid_instruction).to be_falsey
      end
    end

    %w[L R M].each do |instruction|
      context "#{instruction} is valid" do
        subject(:valid_instruction) { rover.valid_instruction?(instruction) }

        it 'returns true' do
          expect(valid_instruction).to be_truthy
        end
      end
    end
  end

  describe 'execute_instruction' do
    subject(:execute_instruction) { rover.execute_instruction(instruction) }

    let(:rover) { FactoryBot.create(:rover, position_x: 1, position_y: 2, orientation: 'N') }

    context 'failure' do
      let(:instruction) { 'Q' }

      it "returns false" do
        expect(execute_instruction).to be_falsey
      end
    end

    %w[L R M].each do |valid_instruction|
      context 'valid command' do
        let(:instruction) { valid_instruction }

        it "returns true" do
          expect(execute_instruction).to be_truthy
        end
      end
    end
  end

end
