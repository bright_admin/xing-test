Rails.application.routes.draw do
  root to: "missions#new"

  resources :missions, only: %i[index create new show]
end
