require 'outcome'

module Missions
  class Generator < Missions::Base
    attr_reader :info

    class << self
      def generate_mission(info)
        new(info).generate_mission
      end
    end

    def initialize(info)
      @info = Missions::InfoParser.parse(info)
    end

    def generate_mission
      ActiveRecord::Base.transaction do
        create_plateau
        create_mission
        create_rovers
      end

      Outcome.new(@mission, @errors)
    end

    def create_plateau
      plateau = Plateau.new(@info.plateau_bounds)
      return add_record_for_record(plateau) unless plateau.save

      @plateau = plateau
    end

    def create_mission
      mission = Mission.new(plateau: @plateau)
      return add_record_for_record(mission) unless mission.save

      @mission = mission
    end

    def create_rovers
      return unless @mission.present?

      @info.rovers.each do |attributes|
        rover = Rover.new(attributes.merge(mission_id: @mission.id))
        return  add_record_for_record(rover) unless rover.save

        rover
      end

      @mission.rovers
    end

    private

    def add_record_for_record(record)
      add_errors(*record.errors.full_messages)
    end
  end
end
