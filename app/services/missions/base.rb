module Missions
  class Base
    attr_reader :errors

    protected

    def add_errors(*messages)
      @errors ||= []
      @errors.push(*messages)

      false
    end
  end
end
