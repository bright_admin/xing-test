# frozen_string_literal: true

module Missions
  class InfoParser
    attr_reader :info, :plateau_bounds

    class << self
      def parse(info)
        new(info).parse
      end
    end

    def initialize(info)
      @info = info || ''
      split_lines
    end

    def parse
      OpenStruct.new(rovers: rovers, plateau_bounds: plateau_bounds)
    end

    def plateau_bounds
      points = @plateu_line.split('').map(&:to_i)
      { max_x: points[0], max_y: points[1] }
    end

    def rovers
      _rovers = []

      @rover_lines.each_slice(2) do |position, instructions|
        position = position.split('')
        _rovers.push(
          position_x: position[0].to_i,
          position_y: position[1].to_i,
          orientation: position[2],
          instructions: instructions
        )
      end

      _rovers
    end

    private

    def split_lines
      @lines = @info.split("\n").map{|line| line.gsub(/\s+/, "") }.reject(&:empty?)
      @plateu_line = @lines[0] || ''
      @rover_lines = @lines.slice(1, @lines.length) || []
    end
  end
end
