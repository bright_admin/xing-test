class Plateau < ApplicationRecord
  validates :max_x, :max_y, numericality: { greater_than: 0 }
end
