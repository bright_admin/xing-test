#use of self for readability / references sake

module NavigationSystem
  POSSIBLE_INSTRUCTIONS = {
      L: 'turn_left',
      R: 'turn_right',
      M: 'move_forward',
    }.freeze

  {
    turn_left: { N: 'W', W: 'S', S: 'E', E: 'N' },
    turn_right: { N: 'E', E: 'S', S: 'W', W: 'N' }
  }.each do |turn, cardinals|
    define_method(turn) do
      self.update(orientation: cardinals[self.orientation.to_sym])
    end
  end

  def move_forward
   grid_step = { N: [0,1], W: [-1,0], S: [0,-1], E: [1,0] }

   x = self.position_x + grid_step[self.orientation.to_sym][0]
   y = self.position_y + grid_step[self.orientation.to_sym][1]

   self.update(position_x: x, position_y: y)
  end
end
