module Plateaus
  class WithinBoundsValidator < ActiveModel::Validator
    def validate(record)
      @record = record

      record.errors[@record.class.name.to_sym].push('out of bounds') unless within_bounds?
    end

    private

    def within_bounds?
      plateau = @record.plateau

      @record.position_x.between?(plateau.min_x, plateau.max_x) && @record.position_y.between?(plateau.min_y, plateau.max_y)
    end
  end
end
