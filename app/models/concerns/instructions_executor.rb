module InstructionsExecutor
  extend ActiveSupport::Concern

  module ClassMethods
    def execute_instructions
      self.all.each { |rover| rover.execute_instructions }
    end
  end

  def execute_instructions
    return unless self.instructions.present?
    list = self.instructions.split('')
    list.each { |instruction| execute_instruction(instruction) }
  end

  def execute_instruction(instruction)
    return unless valid_instruction?(instruction)

    command = possible_instructions[instruction.to_sym]
    self.send(command)
  end

  def valid_instruction?(instruction)
    possible_instructions.keys.include?(instruction.to_sym)
  end

  private

  def possible_instructions
    self.class.name.constantize::POSSIBLE_INSTRUCTIONS
  end
end
