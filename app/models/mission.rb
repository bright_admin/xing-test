class Mission < ApplicationRecord
  belongs_to :plateau
  has_many :rovers

  before_create :set_mission_name

  private

  def set_mission_name
    self.name = "Mission #{self.class.last&.id.to_i + 1}"
  end
end
