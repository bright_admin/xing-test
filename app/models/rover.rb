class Rover < ApplicationRecord
  include NavigationSystem
  include InstructionsExecutor

  belongs_to :mission

  validates_presence_of :position_x, :position_y, :orientation
  validates_with Plateaus::WithinBoundsValidator, if: :coordinates_changed?

  def plateau
    mission.plateau
  end

  private

  def coordinates_changed?
    (position_x.present? && position_y.present?) && (position_x_changed? || position_y_changed?)
  end
end
