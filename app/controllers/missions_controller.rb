class MissionsController < ApplicationController
  before_action :find_mission!, only: %i[show]

  def new; end

  def show; end

  def create
    outcome = Missions::Generator.generate_mission(params[:mission_info])

    return redirect_back_response(outcome.errors.join(', '), success: false) unless outcome.success?

    mission = outcome.result
    mission.rovers.execute_instructions

    redirect_to mission_path(mission.id)
  end

  private

  def find_mission!
    @mission = Mission.find_by_id(params[:id])
    return redirect_back_response('Mission not found', success: false) unless @mission
  end
end
