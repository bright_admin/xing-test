module ResponseHelper
  def redirect_back_response(message, success: true)
    flash_type = success ? 'succes' : 'error'
    flash[flash_type.to_sym] = message
    redirect_back(fallback_location: root_path)
  end
end
